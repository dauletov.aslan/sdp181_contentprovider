package kz.astana.contentproviderexample;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class MyContentProvider extends ContentProvider {

    private final String TABLE_NAME = "animals";

    private final String AUTHORITY = "kz.astana.contentprovider.my";
    private final String ANIMAL_PATH = "animals";
    private final Uri ANIMAL_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + ANIMAL_PATH);
    private final String ANIMAL_CONTENT_TYPE_DIR = "vnd.android.cursor.dir/vnd." + AUTHORITY + "." + ANIMAL_PATH;
    private final String ANIMAL_CONTENT_TYPE_ITEM = "vnd.android.cursor.item/vnd." + AUTHORITY + "." + ANIMAL_PATH;

    private final UriMatcher uriMatcher;
    private final int URI_ANIMALS = 1;
    private final int URI_ANIMALS_ID = 2;

    private SQLiteDatabase database;
    private MyDatabaseHelper databaseHelper;

    public MyContentProvider() {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, ANIMAL_PATH, URI_ANIMALS);
        uriMatcher.addURI(AUTHORITY, ANIMAL_PATH + "/#", URI_ANIMALS_ID);
    }

    @Override
    public boolean onCreate() {
        databaseHelper = new MyDatabaseHelper(getContext(), "database", null, 1);
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d("Hello", "Query: " + uri.toString());
        switch (uriMatcher.match(uri)) {
            case URI_ANIMALS:
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = "name ASC";
                }
                break;
            case URI_ANIMALS_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    selection = "_id = " + id;
                } else {
                    selection += " AND _id = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        database = databaseHelper.getReadableDatabase();
        Cursor cursor = database.query(TABLE_NAME, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), ANIMAL_CONTENT_URI);
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.d("Hello", "Insert: " + uri);
        if (uriMatcher.match(uri) != URI_ANIMALS) {
            throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        database = databaseHelper.getWritableDatabase();
        long rowId = database.insert(TABLE_NAME, null, values);
        Uri resultUri = ContentUris.withAppendedId(ANIMAL_CONTENT_URI, rowId);
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.d("Hello", "Update: " + uri);
        switch (uriMatcher.match(uri)) {
            case URI_ANIMALS:
                break;
            case URI_ANIMALS_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    selection = "_id = " + id;
                } else {
                    selection += " AND _id = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        database = databaseHelper.getWritableDatabase();
        int updateCount = database.update(TABLE_NAME, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return updateCount;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.d("Hello", "Delete: " + uri);
        switch (uriMatcher.match(uri)) {
            case URI_ANIMALS:
                break;
            case URI_ANIMALS_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    selection = "_id = " + id;
                } else {
                    selection = " AND _id" + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        database = databaseHelper.getWritableDatabase();
        int deleteCount = database.delete(TABLE_NAME, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return deleteCount;
    }

    @Override
    public String getType(Uri uri) {
        Log.d("Hello", "Get type: " + uri);
        switch (uriMatcher.match(uri)) {
            case URI_ANIMALS:
                return ANIMAL_CONTENT_TYPE_DIR;
            case URI_ANIMALS_ID:
                return ANIMAL_CONTENT_TYPE_ITEM;
        }
        return null;
    }
}