package kz.astana.contentproviderexample;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private final Uri ANIMAL_URI = Uri.parse("content://kz.astana.contentprovider.my/animals");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText id = findViewById(R.id.idEditText);
        EditText name = findViewById(R.id.nameEditText);

        ListView listView = findViewById(R.id.listView);

        Cursor cursor = getContentResolver().query(ANIMAL_URI, null, null, null, null);
        startManagingCursor(cursor);

        String[] from = {"_id", "name"};
        int[] to = {android.R.id.text1, android.R.id.text2};
        SimpleCursorAdapter simpleCursorAdapter =
                new SimpleCursorAdapter(
                        MainActivity.this,
                        android.R.layout.simple_list_item_2,
                        cursor,
                        from,
                        to,
                        SimpleCursorAdapter.IGNORE_ITEM_VIEW_TYPE
                );
        listView.setAdapter(simpleCursorAdapter);

        Button insert = findViewById(R.id.insertButton);
        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("name", name.getText().toString());
                Uri uri = getContentResolver().insert(ANIMAL_URI, contentValues);
                Log.d("Hello", "Inserted, Uri: " + uri.toString());
            }
        });

        Button update = findViewById(R.id.updateButton);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContentValues contentValues = new ContentValues();
                contentValues.put("name", name.getText().toString());
                Uri uri = ContentUris.withAppendedId(ANIMAL_URI, Long.parseLong(id.getText().toString()));
                int countUpdate = getContentResolver().update(uri, contentValues, null, null);
                Log.d("Hello", "Updated: " + countUpdate);
            }
        });

        Button delete = findViewById(R.id.deleteButton);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = ContentUris.withAppendedId(ANIMAL_URI, Long.parseLong(id.getText().toString()));
                int countDelete = getContentResolver().delete(uri, null, null);
                Log.d("Hello", "Updated: " + countDelete);
            }
        });
    }
}